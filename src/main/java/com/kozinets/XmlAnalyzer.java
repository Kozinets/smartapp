package com.kozinets;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class XmlAnalyzer {

    private static final String TARGET_ELEMENT_BY_ID = "make-everything-ok-button";

    private static final String CHARSET = "UTF8";

    public static void main(String[] args) throws IOException {

        List<File> files = Stream.of(args).limit(2).filter(x -> {
            if (args.length > 3 || args.length < 2) throw new IllegalArgumentException();
            return true;
        }).map(File::new).collect(Collectors.toList());

        Document originFile = Jsoup.parse(files.get(0), CHARSET, files.get(0).getAbsolutePath());
        Document changedFile = Jsoup.parse(files.get(1), CHARSET, files.get(1).getAbsolutePath());

        String targetElementById = args.length == 3 ? args[2] : TARGET_ELEMENT_BY_ID;
        Optional.ofNullable(originFile.getElementById(targetElementById))
                .orElseThrow(NoSuchElementException::new);

        List<Attribute> attributes = originFile.getElementById(targetElementById)
                .attributes().asList();

        List<String> targetValues = attributes.stream().map(Attribute::getValue)
                .collect(Collectors.toList());

        List<Element> matchedElements = attributes.stream()
                .map(a -> changedFile.getElementsByAttributeValue(a.getKey(), a.getValue()))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        Element result = null;

        for (Element element : matchedElements) {
            for (Attribute attribute : element.attributes()) {
                if (targetValues.contains(attribute.getValue())) {
                    result = element;
                }
            }
        }

        if (result == null) {
            System.out.println("Element wasn't found");
        } else {
            System.out.println(getXPath(result));
        }
    }

    private static String getXPath(Element element) {
        Elements parents = element.parents();
        Collections.reverse(parents);
        parents.add(element);
        return parents.stream()
                .map(el -> el.nodeName() + "[" + el.elementSiblingIndex() + "]")
                .collect(Collectors.joining(" > "));
    }
}
